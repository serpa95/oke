﻿using OKE.ReservationSlot.Core;
using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OKE.ReservationSlot.Controllers
{
    public class ReservationController : ApiController
    {
        private readonly IReservationService _reservationService;
        private readonly IUserService _userService;
        public ReservationController(IReservationService reservationService, IUserService userService)
        {
            _reservationService = reservationService;
            _userService = userService;
        }
        [HttpPost]
        public IHttpActionResult AddNewReservation([FromBody] Reservation reservation)
        {
            try
            {
                // TO DO
                // reservation.UserId = HttpContext.Current.User.Identity
                _reservationService.AddReservation(reservation);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        public IHttpActionResult DeleteReservation(int reservationId)
        {
            try
            {
                _reservationService.DeleteReservation(reservationId);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IHttpActionResult GetReservationByRoom(int roomId, DateTime day)
        {
            try
            {
                var reservation = _reservationService.GetRoomReservationsPerDay(roomId, day);
                return Ok(reservation);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IHttpActionResult GetMyReservation(int userId)
        {
            try
            {
                var reservations = _reservationService.GetMyReservation(userId);
                return Ok(reservations);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IHttpActionResult GetReservations()
        {
            try
            {
                var reservations = _reservationService.GetReservations();
                return Ok(reservations);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IHttpActionResult GetReservation(int reservationId)
        {
            try
            {
                var reservation = _reservationService.GetReservationById(reservationId);
                return Ok(reservation);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}