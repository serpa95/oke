﻿using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Core
{
    public interface IReservationService
    {
        void AddReservation(Reservation reservation);
        void DeleteReservation(int reservationId);
        Reservation GetReservationById(int reservationId);
        Reservation GetReservationByName(string reservationName);
        ICollection<Reservation> GetRoomReservationsPerDay(int roomId, DateTime day);
        ICollection<Reservation> GetRoomReservations(int roomId);
        ICollection<Reservation> GetMyReservation(int userId);
        ICollection<Reservation> GetReservations();
    }
}