﻿using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Core
{
    public interface IRoomService
    {
        void AddRoom(Room room);
        void EditRoom(Room room);
        void DeleteRoom(int roomId);
        Room GetRoomById(int roomId);
        Room GetRoomByName(string roomName);
        ICollection<Room> GetRooms();
    }
}
