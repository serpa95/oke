﻿using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Core
{
    public interface IUserService
    {
        void AddUser(User user);
        void EditUser(User user);
        void DeleteUser(int userId);
        User GetUserByName(string userName);
        User GetUserById(int userId);
        ICollection<User> GetUsers();
    }
}