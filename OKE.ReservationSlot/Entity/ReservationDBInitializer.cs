﻿using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace OKE.ReservationSlot.Entity
{
    public class ReservationDBInitializer : DropCreateDatabaseAlways<ReservationDbContext>
    {
        protected override void Seed(ReservationDbContext context)
        {
            IList<User> defaultUsers = new List<User>();
            defaultUsers.Add(new User() { Id = 1, Name = "User 1" });
            defaultUsers.Add(new User() { Id = 2, Name = "User 2" });
            defaultUsers.Add(new User() { Id = 3, Name = "User 3" });
            context.Users.AddRange(defaultUsers);

            IList<Room> defaultRooms = new List<Room>();
            defaultRooms.Add(new Room() { Id = 1, Name = "Room 1" });
            defaultRooms.Add(new Room() { Id = 2, Name = "Room 2" });
            defaultRooms.Add(new Room() { Id = 3, Name = "Room 3" });
            context.Rooms.AddRange(defaultRooms);

            IList<Reservation> defaultReservations = new List<Reservation>();
            defaultReservations.Add(new Reservation() { Id = 1, Name = "Reservation 1", Room = defaultRooms[0], User = defaultUsers[0], From = DateTime.Now, To = DateTime.Now.AddHours(1) });
            defaultReservations.Add(new Reservation() { Id = 2, Name = "Reservation 2", Room = defaultRooms[1], User = defaultUsers[1], From = DateTime.Now, To = DateTime.Now.AddHours(1) });
            defaultReservations.Add(new Reservation() { Id = 3, Name = "Reservation 3", Room = defaultRooms[1], User = defaultUsers[0], From = DateTime.Now, To = DateTime.Now.AddHours(1) });
            defaultReservations.Add(new Reservation() { Id = 5, Name = "Reservation 4", Room = defaultRooms[2], User = defaultUsers[0], From = DateTime.Now.AddHours(-1), To = DateTime.Now });
            defaultReservations.Add(new Reservation() { Id = 6, Name = "Reservation 5", Room = defaultRooms[2], User = defaultUsers[1], From = DateTime.Now, To = DateTime.Now.AddMinutes(30) });
            defaultReservations.Add(new Reservation() { Id = 7, Name = "Reservation 6", Room = defaultRooms[2], User = defaultUsers[2], From = DateTime.Now.AddHours(1), To = DateTime.Now.AddHours(2) });
            context.Reservations.AddRange(defaultReservations);


            base.Seed(context);
        }
    }
}