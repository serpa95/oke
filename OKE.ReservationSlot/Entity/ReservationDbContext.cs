﻿using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Entity
{
    public class ReservationDbContext : DbContext
    {
        public ReservationDbContext() : base()
        {
            Database.SetInitializer(new ReservationDBInitializer());
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
    }
}