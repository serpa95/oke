﻿using Autofac;
using Autofac.Integration.WebApi;
using OKE.ReservationSlot.Core;
using OKE.ReservationSlot.Entity;
using OKE.ReservationSlot.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace OKE.ReservationSlot.App_Start
{
    public class AutoFacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            builder.RegisterWebApiModelBinderProvider();
            builder.RegisterType<UserService>().InstancePerDependency().As<IUserService>();
            builder.RegisterType<RoomService>().InstancePerDependency().As<IRoomService>();
            builder.RegisterType<ReservationService>().InstancePerDependency().As<IReservationService>();
            builder.RegisterType<ReservationDbContext>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}