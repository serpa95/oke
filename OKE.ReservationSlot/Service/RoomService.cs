﻿using OKE.ReservationSlot.Core;
using OKE.ReservationSlot.Entity;
using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Service
{
    public class RoomService : IRoomService
    {
        private readonly ReservationDbContext _context;
        public RoomService(ReservationDbContext context)
        {
            _context = context;
        }
        public void AddRoom(Room room)
        {
            if (GetRoomByName(room.Name) != null)
            {
                throw new Exception("Room already exist");
            }
            _context.Rooms.Add(room);
            _context.SaveChanges();
        }

        public void DeleteRoom(int roomId)
        {
            var room = GetRoomById(roomId);
            if (room == null)
            {
                throw new Exception("Room not found");
            }
            _context.Rooms.Remove(room);
            _context.SaveChanges();
        }

        public void EditRoom(Room room)
        {
            _context.Rooms.Attach(room);
            _context.Entry(room).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public Room GetRoomById(int roomId)
        {
            return _context.Rooms.SingleOrDefault(_room => _room.Id == roomId);
        }

        public Room GetRoomByName(string roomName)
        {
            return _context.Rooms.SingleOrDefault(_room => _room.Name == roomName);
        }

        public ICollection<Room> GetRooms()
        {
            return _context.Rooms.ToList();
        }
    }
}
