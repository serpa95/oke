﻿using OKE.ReservationSlot.Core;
using OKE.ReservationSlot.Entity;
using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Service
{
    public class UserService : IUserService
    {
        private readonly ReservationDbContext _context;
        public UserService(ReservationDbContext context)
        {
            _context = context;
        }
        public void AddService(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public void AddUser(User user)
        {
            if (GetUserByName(user.Name) != null)
            {
                throw new Exception("User already exist");
            }

            _context.Users.Add(user);
            _context.SaveChanges();
        }
        public void EditUser(User user)
        {
            _context.Users.Attach(user);
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public void DeleteUser(int userId)
        {
            var user = GetUserById(userId);
            if (user == null)
            {
                throw new Exception("User not found");
            }
            _context.Users.Remove(user);
        }

        public User GetUserById(int userId)
        {
            return _context.Users.SingleOrDefault(_user => _user.Id == userId);
        }

        public User GetUserByName(string userName)
        {
            return _context.Users.SingleOrDefault(_user => _user.Name == userName);
        }

        public ICollection<User> GetUsers()
        {
            return _context.Users.ToList();
        }

    }
}