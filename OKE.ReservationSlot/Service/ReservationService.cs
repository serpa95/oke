﻿using OKE.ReservationSlot.Core;
using OKE.ReservationSlot.Entity;
using OKE.ReservationSlot.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OKE.ReservationSlot.Service
{
    public class ReservationService : IReservationService
    {
        private readonly ReservationDbContext _context;
        public ReservationService(ReservationDbContext context)
        {
            _context = context;
        }
        public void AddReservation(Reservation reservation)
        {
            var reservations = GetRoomReservations(reservation.RoomId);
            var overlappingReservation = reservations.Count(_reservation =>
                ((reservation.From >= _reservation.From) &&
                (reservation.From < _reservation.To)) ||
                ((reservation.To > _reservation.From) &&
                (reservation.To <= _reservation.To))
                );
            if (overlappingReservation > 0)
            {
                throw new Exception("Can not add this reservation");
            }
            _context.Reservations.Add(reservation);
            _context.SaveChanges();
        }

        public void DeleteReservation(int reservationId)
        {
            var reservation = GetReservationById(reservationId);
            if (reservation == null)
            {
                throw new Exception("Reservation not found");
            }
            _context.Reservations.Remove(reservation);
            _context.SaveChanges();
        }

        public ICollection<Reservation> GetMyReservation(int userId)
        {
            return _context.Reservations.Where(_reservation => _reservation.UserId == userId).ToList();
        }

        public ICollection<Reservation> GetReservations()
        {
            return _context.Reservations.ToList();
        }

        public Reservation GetReservationById(int reservationId)
        {
            return _context.Reservations.Include(c => c.Room).SingleOrDefault(_reservation => _reservation.Id == reservationId);
        }

        public Reservation GetReservationByName(string reservationName)
        {
            return _context.Reservations.Include(c => c.Room).SingleOrDefault(_reservation => _reservation.Name == reservationName);
        }

        public ICollection<Reservation> GetRoomReservationsPerDay(int roomId, DateTime day)
        {
            return _context.Reservations.Include(c => c.Room).Where(_reservation => (DbFunctions.TruncateTime(_reservation.From) == day.Date || DbFunctions.TruncateTime(_reservation.To) == day.Date) && _reservation.RoomId == roomId).ToList();
        }
        public ICollection<Reservation> GetRoomReservations(int roomId)
        {
            return _context.Reservations.Include(c => c.Room).Where(_reservation => _reservation.RoomId == roomId).ToList();
        }
    }
}